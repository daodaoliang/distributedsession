package com.darkidiot.session.conf;

import lombok.Data;
/**
 * 配置类
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
@Data
public class Configuration {
    /** 配置数据库类型，默认redis */
    private String source = "redis";
    /** 配置序列化类型，支持 json, binary 默认json */
    private SerializeType serializeType = SerializeType.json;
    /** 配置session在redis存放的key前缀 */
    private String sessionRedisPrefix = "distributed-session";
    /** 配置是否集群，默认False */
    private Boolean sessionRedisCluster = false;
    /** 配置获取连接前是否先测试 */
    private Boolean sessionRedisTestOnBorrow = true;
    /** 配置最大空闲进程数 */
    private Integer sessionRedisMaxIdle = 2;
    /** 配置最大进程数 */
    private Integer sessionRedisMaxTotal = 5;
    /** 配置redis服务器地址 */
    private String sessionRedisHost = "127.0.0.1";
    /** 配置redis端口 */
    private Integer sessionRedisPort = 6379;
    /** 配置redis存放的db的index */
    private Integer sessionRedisDbIndex = 0;
    /** 配置集群监控master名称 */
    private String sessionRedisSentinelHosts;
    /** 配置集群监控服务器地址(多个以逗号分隔)如 127.0.0.1:26379,192.1.1.1:26379 */
    private String sessionRedisSentinelMasterName;
}
