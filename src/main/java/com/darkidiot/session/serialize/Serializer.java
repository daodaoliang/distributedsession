package com.darkidiot.session.serialize;

import com.darkidiot.session.exception.SerializableException;

import java.util.Map;

/**
 * 序列化接口类
 * session attribute持久化到redis
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public interface Serializer {

    /**
     * 序列化接口
     * @param paramObject
     * @return
     */
    String serialize(Map<String, Object> paramObject);

    /**
     * 反序列化接口
     * @param paramString
     * @return
     * @throws SerializableException
     */
    Map<String, Object> deserialize(String paramString) throws SerializableException;
}
